# My Store

My Store allows you create, edit or remove items. 

## Technologies
- Backend: PHP 7.2 and Laravel 5.6
- Frontend: JavaScript and jQuery 3.2

## Installation
Before the installation you will need to have Docker already installed. Go to https://www.docker.com/get-started.

Then run the following steps:

- `git clone https://ctkc@bitbucket.org/ctkc/my-store-project.git`

- `cd my-store-project`

- `cp .env.example .env`

- Into *.env*, complete *DB_NAME*, *DB_USERNAME* and *DB_PASSWORD* with your information.

Into the project folder execute the *production.sh* script

- `./production.sh`

This script will execute Docker and make the build for production.

Now, the project is available in *http://localhost*

If you prefer you can modify the *.env* file for any environment and then make the build manually: 

- `docker-compose up -d`

- `docker-compose run --rm app composer install (--optimize-autoloader --no-dev FOR PRODUCTION)`

- `docker-compose run --rm app php artisan config:cache`

- `docker-compose run --rm assets npm install`

- `docker-compose run --rm assets npm run ENVIRONMENT`

You can execute `./production.sh stop` or `docker-compose down` to stop all containers.