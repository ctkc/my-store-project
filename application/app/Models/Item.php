<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Item extends Eloquent
{
    protected $fillable = [
        'img',
        'name',
        'description',
        'rank'
    ];

    protected $appends = ['img_public_url'];

    public function getImgPublicUrlAttribute()
    {
        return config('constants.images_path') . '/' . $this->img;
    }
}
