<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;

class ItemsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $items = Item::orderBy('rank')->get();

            return response()->json(['success' => true, 'items' => $items]);
        } catch (\Exception $e) {
            Log::error("Api/ItemsController@index: {$e->getMessage()}");

            return response()->json([
                'success' => false,
                'message' => 'An error has occurred trying to fetch your items. Please try again.'
            ]);
        }
    }

    /**
     * Store a new item.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = $this->getValidationFactory()->make($request->all(), [
                'img'         => 'required|mimes:jpeg,gif,png',
                'name'        => 'required',
                'description' => 'required',
                'rank'        => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'message' => $validator->errors()->first()]);
            }

            if (!File::exists(public_path(config('constants.images_path')))) {
                File::makeDirectory(public_path(config('constants.images_path')), 0777, true);
            }

            $imgName = pathinfo($request->file('img')->getClientOriginalName(), PATHINFO_FILENAME);
            $imgName = $imgName . '_' . strtotime('now') . '.jpg';

            Image::make($request->file('img'))->save(public_path() . config('constants.images_path') . "/{$imgName}", 100);

            $item = Item::create([
                'img'         => $imgName,
                'name'        => $request->name,
                'description' => $request->description,
                'rank'        => $request->rank
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Your item has been added successfully.',
                'item' => $item
            ]);

        } catch (\Exception $e) {
            Log::error("Api/ItemsController@create: {$e->getMessage()}");

            return response()->json([
                'success' => false,
                'message' => 'An error has occurred trying to create your item. Please try again.'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $item = Item::find($id);

            return response()->json(['success' => true, 'item' => $item]);
        } catch (\Exception $e) {
            Log::error("Api/ItemsController@show: {$e->getMessage()}");
            return response()->json(['success' => false, 'message' => 'An error has occurred trying to retrieve this item. Please try again.']);
        }
    }

    /**
     * Update the specified item.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $validator = $this->getValidationFactory()->make($request->all(), [
                'id'          => 'required',
                'img'         => 'mimes:jpeg,gif,png',
                'name'        => 'required',
                'description' => 'required'
            ]);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'message' => $validator->errors()->first()]);
            }

            $item = Item::find($request->id);

            $fields = ['name' => $request->name, 'description' => $request->description];
            if ($request->file('img', false)) {
                if (File::exists(public_path() . $item->img_public_url)) {
                    File::delete(public_path() . $item->img_public_url);
                }

                $imgName = pathinfo($request->file('img')->getClientOriginalName(), PATHINFO_FILENAME);
                $imgName = $imgName . '_' . strtotime('now') . '.jpg';

                Image::make($request->file('img'))->save(public_path() . config('constants.images_path') . "/{$imgName}", 100);

                $fields = array_merge($fields, ['img' => $imgName]);
            }

            $item->update($fields);

            return response()->json([
                'success' => true,
                'message' => 'Your item has been saved successfully.',
                'item' => $item
            ]);

        } catch (\Exception $e) {
            Log::error("Api/ItemsController@update: {$e->getMessage()}");

            return response()->json([
                'success' => false,
                'message' => 'An error has occurred trying to edit your item. Please try again.'
            ]);
        }
    }

    /**
     * Remove specific item.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $item = Item::find($id);

            File::delete(public_path() . '/img/items/' . $item->img);

            $item->delete();

            return response()->json(['success' => true, 'message' => 'The item has been deleted successfully.']);
        } catch (\Exception $e) {
            Log::error("Api/ItemsController@destroy: {$e->getMessage()}");
            return response()->json(['success' => false, 'message' => 'An error has occurred trying to delete this item. Please try again.']);
        }
    }

    /**
     * Search items.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        if (strlen(trim($request->term)) <= 2) {
            $items = Item::orderBy('rank')->get();
        } else {
            $items = Item::where('name', 'LIKE',"%{$request->term}%")->get();
        }

        return response()->json(['success' => true, 'items' => $items]);
    }

    public function saveOrder(Request $request)
    {
        try {
            $items = Item::all()->getDictionary();
            foreach ($request->ids as $key => $id) {
                $items[$id]->update(['rank' => ++$key]);
            }

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            Log::error("Api/ItemsController@saveOrder: {$e->getMessage()}");

            return response()->json(['success' => false, 'message' => 'An error has occurred trying to reorder your items. Please try again.']);
        }
    }
}
