import 'script-loader!./../../../../node_modules/sortablejs'
import axios from 'axios'
import toggleLoader from './../utils/loader'
import { error, success, confirmation } from './../utils/alerts'
import { initCounter } from './../utils/textCounter'

class HomePage {
  constructor(getItems = true) {
    this.addItemModal = $('.add-item-wrapper')
    this.itemTemplate = $('.item.hidden')
    this.searchWrapper = $('.search-item')
    this.itemsWrapper = $('.items-wrapper')

    this.itemsWrapper.on('click', '.item__move', e => e.stopPropagation())
    this.itemsWrapper.on('click', '.item__delete', e => this.confirmDelete(e))

    let timer = null
    let func = this.searchItem
    const _this = this
    this.searchWrapper.on('keydown', 'input', e => {
      clearTimeout(timer)
      if (e.keyCode == 13) {
        e.preventDefault()
        func(e, _this)
      } else {
        timer = setTimeout(() => func(e, _this), 250)
      }
    })

    if (getItems) {
      this.getItems()
      this.checkIfThereAreItems()
      this.initSort()
      toggleLoader()
    }
  }

  async getItems () {
    try {
      const response = await axios.get('/api/items')
      if (!response.data.success) {
        error(response.data.message)

        return false
      }

      response.data.items.forEach(item => this.addNewItem(item))
      this.updateCounter()
      toggleLoader()

    } catch (e) {
      console.error(e)

      error('An error has occurred trying to retrieve the items.')
    }
  }

  addNewItem (item) {
    let itemTemplate = this.itemTemplate.clone()
    itemTemplate.find('.item__img img')
        .attr('src', item.img_public_url)
        .attr('alt', item.img)
    itemTemplate.find('.item__body h4').html(item.name)
    itemTemplate.find('.item__body p').html(item.description)
    itemTemplate.attr('data-id', item._id)

    this.itemsWrapper.append(itemTemplate.removeClass('hidden'))
    this.updateCounter()
    this.checkIfThereAreItems()
  }

  confirmDelete (e) {
    e.stopPropagation()

    let element = $(e.target).parent()
    confirmation('The item will be deleted permanently!', () => this.deleteItem(element))
  }

  async deleteItem (element) {
    let id = element.parents('.item').data('id')
    try {
      const response = await axios.delete(`/api/items/${id}`)
      if (!response.data.success) {
        error(response.data.message)

        return false
      }

      element.parents('.item').remove()
      this.updateCounter()
      this.checkIfThereAreItems()
      success(response.data.message)

    } catch (e) {
      console.error(e)

      error('An error has occurred trying to delete this item. Please try again.')
    }
  }

  checkIfThereAreItems () {
    if (this.itemsWrapper.find('.item').length > 0) {
      this.itemsWrapper.find('.no-items').addClass('hidden')
    } else {
      this.itemsWrapper.find('.no-items').removeClass('hidden')
    }
  }

  async searchItem (e, _this) {
    let term = $(e.target).val()
    try {
      const response = await axios.get(`/api/items/search?term=${term}`)
      if (!response.data.success) {
        return false
      }

      $('.items-wrapper').children('.item').remove()
      response.data.items.forEach(item => _this.addNewItem(item))
      _this.checkIfThereAreItems()

    } catch (e) {
      console.error(e)
    }
  }

  initSort () {
    let container = document.getElementsByClassName('items-wrapper')[0]
    Sortable.create(container, {
      handle: '.item__move',
      animation: 250,
      draggable: '.item',
      onEnd: () => this.saveOrder()
    })
  }

  async saveOrder () {
    let items = this.itemsWrapper.find('.item')
    let ids = items.map(function () {
      return $(this).data('id')
    }).get()

    try {
      const response = await axios.post('/api/items/save-order', {ids})
      if (!response.data.success) {
        error(response.data.message)

        return false
      }
    } catch (e) {
      console.error(e)
      error('An error has occurred trying to reorder your items. Please try again.')
    }
  }

  updateCounter () {
    let total = this.itemsWrapper.find('.item').length
    this.itemsWrapper.find('.total-items').html(`Total Items: ${total}`)
  }
}

export default HomePage