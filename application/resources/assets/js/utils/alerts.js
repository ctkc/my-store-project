export function error(content) {
  $.alert({
    theme: 'modern',
    title: 'Whoops!',
    content,
    type: 'red',
    draggable: false,
    animateFromElement: false
  })
}

export function success(content) {
  $.alert({
    theme: 'modern',
    title: 'Great!',
    content,
    type: 'green',
    draggable: false,
    animateFromElement: false
  })
}

export function confirmation(content, callback) {
  $.confirm({
    theme: 'modern',
    type: 'orange',
    draggable: false,
    animateFromElement: false,
    title: 'Are you really sure?',
    content,
    buttons: {
      confirm: () => callback(),
      cancel: () => {}
    }
  })
}

export function warning(content) {
  $.alert({
    theme: 'modern',
    title: 'Whoops!',
    content,
    type: 'orange',
    draggable: false,
    animateFromElement: false
  })
}