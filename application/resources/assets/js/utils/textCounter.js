export function initCounter(element) {
  $(element).textcounter({
    countDownText: "%d",
    countDown: true,
    countSpaces: true,
    max: 300
  })
}

export function removeCounter(element) {
  $.removeData($(element).get(0))
  $(element).siblings('.text-count-wrapper').remove()
}