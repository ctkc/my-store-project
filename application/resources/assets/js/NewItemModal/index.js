import axios from 'axios'
import { error, warning } from './../utils/alerts'
import { removeCounter } from './../utils/textCounter'
import { initCounter } from '../utils/textCounter'

class NewItemModal {
  constructor () {
    this.itemsWrapper = $('.items-wrapper')
    this.itemTemplate = $('.item.hidden')
    this.addItemModal = $('.add-item-wrapper')
    this.createButton = $('.add-new-item')

    this.createButton.on('click', () => this.openAddItemModal())
    this.addItemModal.on('click', '.add-item__close', () => this.closeModal())
    this.addItemModal.on('click', '.add-item__image a', this.openInputFile)
    this.addItemModal.on('change', '.add-item__image input', this.addImage)
    this.addItemModal.on('submit', 'form', e => this.validateAndSubmit(e))
    this.addItemModal.on('change', '.add-item__image input', this.resetValidation)
    this.addItemModal.on('keypress', 'input', this.resetValidation)
    this.addItemModal.on('keypress', 'textarea', this.resetValidation)
  }

  openAddItemModal () {
    this.createButton.addClass('animate')
    setTimeout(() => this.createButton.removeClass('animate'), 400)
    this.addItemModal.find('.item__rank').val(this.itemsWrapper.find('.item').length + 1)
    this.addItemModal.removeClass('hidden')
    initCounter(this.addItemModal.find('.add-item__form textarea'))
  }

  closeModal () {
    this.addItemModal.addClass('hidden')
    this.resetForm()
    removeCounter(this.addItemModal.find('.add-item__form textarea'))
  }

  openInputFile () {
    $(this).next().trigger('click')
  }

  addImage () {
    let input = $(this)
    let file = input.prop('files')[0]

    if (file) {
      let acceptedTypes = ['image/jpeg', 'image/png', 'image/gif']
      if (acceptedTypes.indexOf(file.type) === -1) {
        warning('The image must be of type PNG, JPG or GIF')
        input.val('')
        return false
      }

      // Check image dimensions.
      let image = new Image()

      image.onload = function () {
        if (this.width > 320 || this.height > 320) {
          input.val('')
          input.siblings('a').children('img').attr('src', '/img/empty-photo.jpg')
          warning(`The image width and height must be less than 320px`)
        }
      }

      let _URL = window.URL || window.webkitURL
      image.src = _URL.createObjectURL(file)

      // Generate preview.
      let reader = new FileReader()

      reader.onload = e => {
        input.siblings('a').children('img').attr('src', e.target.result)
      }

      reader.readAsDataURL(file)
    }
  }

  validateAndSubmit (e) {
    e.preventDefault()

    let errors = []

    if (!e.target.img.value) {
      errors.push({field: 'img', message: '<i class="fas fa-times"></i> The image is required.'})
    }

    if (e.target.name.value.trim().length === 0) {
      errors.push({field: 'name', message: '<i class="fas fa-times"></i> The name is required.'})
    }

    if (e.target.description.value.trim().length === 0) {
      errors.push({field: 'description', message: '<i class="fas fa-times"></i> The description is required.'})
    }

    if (errors.length > 0) {
      errors.forEach(error => {
        $(e.target[error.field]).siblings('small').hide()
        $(e.target[error.field]).siblings('span').html(error.message)
        $(e.target[error.field]).parent().addClass('has-error')
      })

      return false
    }

    let formData = new FormData($(e.target)[0])

    formData.append('rank', this.itemsWrapper.find('.item').length + 1)
    this.submitForm(formData)
  }

  async submitForm (result) {
    try {
      const response = await axios({
        method: 'POST',
        url: '/api/items',
        data: result
      })
      if (!response.data.success) {
        error(response.data.message)

        return false
      }

      this.addNewItem(response.data.item)
      this.closeModal()

    } catch (e) {
      console.error(e)

      error('An error has occurred trying to add your item. Please try again.')
    }
  }

  addNewItem (item) {
    let itemTemplate = this.itemTemplate.clone()
    itemTemplate.find('.item__img img')
        .attr('src', item.img_public_url)
        .attr('alt', item.img)
    itemTemplate.find('.item__body h4').html(item.name)
    itemTemplate.find('.item__body p').html(item.description)
    itemTemplate.attr('data-id', item._id)

    this.itemsWrapper.append(itemTemplate.removeClass('hidden'))
    this.updateCounter()
    this.checkIfThereAreItems()
  }

  checkIfThereAreItems () {
    if (this.itemsWrapper.find('.item').length > 0) {
      this.itemsWrapper.find('.no-items').addClass('hidden')
    } else {
      this.itemsWrapper.find('.no-items').removeClass('hidden')
    }
  }

  updateCounter () {
    let total = this.itemsWrapper.find('.item').length
    this.itemsWrapper.find('.total-items').html(`Total Items: ${total}`)
  }

  resetValidation () {
    $(this).parent().removeClass('has-error').children('small').show()
  }

  resetForm () {
    this.addItemModal.find('.form-group').each((index, group) => {
      $(group).removeClass('has-error').children().val('')
      $(group).children('small').show()
      $(group).find('img').attr('src', '/img/empty-photo.jpg')
    })
  }
}

export default NewItemModal