import axios from 'axios'
import toggleLoader from './../utils/loader'

class ShowItemModal {
  constructor () {
    this.itemsWrapper = $('.items-wrapper')
    this.showItemWrapper = $('.show-item-wrapper')

    this.itemsWrapper.on('click', '.item', e => this.getItemAndOpenModal(e))
    this.showItemWrapper.on('click', '.show-item__close', () => this.closeModal())
  }

  async getItemAndOpenModal (e) {
    toggleLoader()
    let id = $(e.target).parents('.item').data('id')

    try {
      const response = await axios.get(`/api/items/${id}`)
      if (!response.data.success) {

        return false
      }

      this.showItemWrapper.find('.show-item__image img').attr('src', response.data.item.img_public_url)
      this.showItemWrapper.find('.show-item__name').html(response.data.item.name)
      this.showItemWrapper.find('.show-item__description').html(response.data.item.description)

      this.showItemWrapper.removeClass('hidden')

      toggleLoader()

    } catch (e) {
      console.error(e)

    }
  }

  closeModal () {
    this.showItemWrapper.addClass('hidden')
  }
}

export default ShowItemModal