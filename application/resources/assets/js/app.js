import 'imports-loader?jQuery=jquery,$=jquery,this=>window!./../../../node_modules/jquery-confirm/js/jquery-confirm'
import 'jquery-confirm/css/jquery-confirm.css'
import 'imports-loader?jQuery=jquery,$=jquery,this=>window!./../../../node_modules/jquery-text-counter/textcounter'

import HomePage from './HomePage'
import NewItemModal from './NewItemModal'
import EditItemModal from './EditItemModal'
import ShowItemModal from './ShowItemModal'

new HomePage()
new NewItemModal()
new EditItemModal()
new ShowItemModal()