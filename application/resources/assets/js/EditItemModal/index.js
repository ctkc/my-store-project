import axios from 'axios'
import toggleLoader from './../utils/loader'
import { warning, error } from './../utils/alerts'
import { initCounter, removeCounter } from '../utils/textCounter'

class EditItemModal {
  constructor () {
    this.editItemModal = $('.edit-item-wrapper')
    this.itemsWrapper = $('.items-wrapper')
    this.itemTemplate = $('.item.hidden')

    this.itemsWrapper.on('click', '.item__edit', e => this.openModal(e))
    this.editItemModal.on('click', '.edit-item__close', () => this.closeModal())
    this.editItemModal.on('click', '.edit-item__image a', this.openInputFile)
    this.editItemModal.on('change', '.edit-item__image input', this.addImage)
    this.editItemModal.on('submit', 'form', e => this.validateAndSubmit(e))
    this.editItemModal.on('change', '.edit-item__image input', this.resetValidation)
    this.editItemModal.on('keypress', 'input', this.resetValidation)
    this.editItemModal.on('keypress', 'textarea', this.resetValidation)
  }

  async openModal (e) {
    e.stopPropagation()
    toggleLoader()
    let id = $(e.target).parents('.item').data('id')

    try {
      const response = await axios.get(`/api/items/${id}`)
      if (!response.data.success) {
        error(response.data.message)

        return false
      }

      this.editItemModal.find('.edit-item__id').val(response.data.item._id)
      this.editItemModal.find('.edit-item__form img')
          .attr('src', response.data.item.img_public_url)
          .attr('alt', response.data.item.img)
      this.editItemModal.find('.edit-item__form input[name="name"]').val(response.data.item.name)
      this.editItemModal.find('.edit-item__form textarea[name="description"]').val(response.data.item.description)

      initCounter(this.editItemModal.find('textarea[name="description"]'))

      toggleLoader()
      this.editItemModal.removeClass('hidden')

    } catch (e) {
      console.error(e)

      error('An error has occurred trying to retrieve this item. Please try again.')
    }
  }

  closeModal () {
    this.editItemModal.addClass('hidden')
    this.resetForm()
    removeCounter(this.editItemModal.find('.edit-item__form textarea'))
  }

  openInputFile () {
    $(this).next().trigger('click')
  }

  addImage () {
    let input = $(this)
    let file = input.prop('files')[0]

    if (file) {
      let acceptedTypes = ['image/jpeg', 'image/png', 'image/gif']
      if (acceptedTypes.indexOf(file.type) === -1) {
        warning('The image must be of type PNG, JPG or GIF')
        input.val('')
        return false
      }

      // Check image dimensions.
      let image = new Image()

      image.onload = function () {
        if (this.width > 320 || this.height > 320) {
          input.val('')
          input.siblings('a').children('img').attr('src', '/img/empty-photo.jpg')
          warning(`The image width and height must be less than 320px`)
        }
      }

      let _URL = window.URL || window.webkitURL
      image.src = _URL.createObjectURL(file)


      // Generate preview.
      let reader = new FileReader()

      reader.onload = e => {
        input.siblings('a').children('img').attr('src', e.target.result)
      }

      reader.readAsDataURL(file)
    }
  }

  validateAndSubmit (e) {
    e.preventDefault()

    let formData = new FormData($(e.target)[0])
    let errors = []

    if (e.target.name.value.trim().length === 0) {
      errors.push({field: 'name', message: '<i class="fas fa-times"></i> The name is required.'})
    }

    if (e.target.description.value.trim().length === 0) {
      errors.push({field: 'description', message: '<i class="fas fa-times"></i> The description is required.'})
    }

    if (errors.length > 0) {
      errors.forEach(error => {
        $(e.target[error.field]).siblings('small').hide()
        $(e.target[error.field]).siblings('span').html(error.message)
        $(e.target[error.field]).parent().addClass('has-error')
      })

      return false
    }

    this.submitForm(formData)
  }

  async submitForm (result) {
    try {
      const response = await axios({
        method: 'POST',
        url: `/api/items/${result.get('id')}`,
        data: result,
        config: { headers: {'Content-Type': 'multipart/form-data' }}
      })

      if (!response.data.success) {
        error(response.data.message)

        return false
      }

      let itemTemplate = this.itemTemplate.clone()
      itemTemplate.find('.item__img img')
          .attr('src', response.data.item.img_public_url)
          .attr('alt', response.data.item.img)
      itemTemplate.find('.item__body h4').html(response.data.item.name)
      itemTemplate.find('.item__body p').html(response.data.item.description)
      itemTemplate.attr('data-id', response.data.item._id)

      this.itemsWrapper.find(`[data-id="${response.data.item._id}"]`).replaceWith(itemTemplate.removeClass('hidden'))

      this.closeModal()
    } catch (e) {
      console.error(e)

      error('An error has occurred trying to add your item. Please try again.')
    }
  }

  resetValidation () {
    $(this).parent().removeClass('has-error').children('small').show()
  }

  resetForm () {
    this.editItemModal.find('.form-group').each((index, group) => {
      $(group).removeClass('has-error').children().val('')
      $(group).children('small').show()
      $(group).find('img').attr('src', '/img/empty-photo.jpg')
    })
  }
}

export default EditItemModal