<div class="add-item-wrapper hidden">
    <div class="add-item">
        <div class="add-item__close">
            <a href="javascript:;"><i class="fas fa-times"></i></a>
        </div>
        <div class="add-item__title">
            <h2>Add New Item</h2>
        </div>
        <div class="add-item__form">
            <form>
                <div class="add-item__image form-group">
                    <a href="javascript:;">
                        <i class="fas fa-plus"></i>
                        <img src="/img/empty-photo.jpg" alt="Upload the item photo">
                        <div class="border"></div>
                    </a>
                    <input type="file" name="img" accept="image/jpeg, image/gif, image/png">
                    <small>Only formats .png, .jpg, .gif, 320px x 320px of size</small>
                    <span class="form-group__error-message">&nbsp;</span>
                </div>

                <div class="form-group">
                    <label for="name">Name <span>*</span></label>
                    <input type="text" name="name">
                    <span class="form-group__error-message">&nbsp;</span>
                </div>

                <div class="form-group">
                    <label for="description">Description <span>*</span></label>
                    <textarea name="description"></textarea>
                    <span class="form-group__error-message">&nbsp;</span>
                </div>

                <div class="submit-area">
                    <button class="add-item__button">Add Item</button>
                </div>
            </form>
        </div>
    </div>
</div>