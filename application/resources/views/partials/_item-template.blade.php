<div class="item hidden" data-id="">
    <div class="item__shadow">
        <div class="item__move">
            <i class="fas fa-arrows-alt item__move"></i>
        </div>
        <a href="javascript:;" class="item__edit" title="Edit">
            <i class="fas fa-pen"></i>
        </a>
        <a href="javascript:;" class="item__delete" title="Delete">
            <i class="fas fa-times"></i>
        </a>
        <div class="item__img">
            <a href="javascript:;" class="item__open-link">
                <i class="fas fa-eye item__eye"></i>
                <img src="" alt="">
            </a>
        </div>
        <div class="item__body">
            <h4></h4>
            <p></p>
        </div>
    </div>
</div>