<div class="edit-item-wrapper hidden">
    <div class="edit-item">
        <div class="edit-item__close">
            <a href="javascript:;"><i class="fas fa-times"></i></a>
        </div>
        <div class="edit-item__title">
            <h2>Edit Item</h2>
        </div>
        <div class="edit-item__form">
            <form enctype="multipart/form-data">
                <input type="hidden" class="edit-item__id" name="id" value="">
                <div class="edit-item__image form-group">
                    <a href="javascript:;">
                        <i class="fas fa-plus"></i>
                        <img src="/img/empty-photo.jpg" alt="Upload the item photo">
                        <div class="border"></div>
                    </a>
                    <input type="file" name="img" accept="image/jpeg, image/gif, image/png">
                    <small>Only formats .png, .jpg, .gif, 320px x 320px of size</small>
                    <span class="form-group__error-message">&nbsp;</span>
                </div>

                <div class="form-group">
                    <label for="name">Name <span>*</span></label>
                    <input type="text" name="name">
                    <span class="form-group__error-message">&nbsp;</span>
                </div>

                <div class="form-group">
                    <label for="description">Description <span>*</span></label>
                    <textarea name="description"></textarea>
                    <span class="form-group__error-message">&nbsp;</span>
                </div>

                <div class="submit-area">
                    <button class="edit-item__button">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>