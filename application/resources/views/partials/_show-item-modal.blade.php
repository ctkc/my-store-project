<div class="show-item-wrapper hidden">
    <div class="show-item">
        <div class="show-item__close">
            <a href="javascript:;"><i class="fas fa-times"></i></a>
        </div>
        <div class="show-item__form">
            <div class="show-item__image form-group">
                <a href="javascript:;">
                    <img src="" alt="">
                </a>
            </div>

            <h1 class="show-item__name"></h1>

            <p class="show-item__description"></p>
        </div>
    </div>
</div>