<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/solid.css" integrity="sha384-wnAC7ln+XN0UKdcPvJvtqIH3jOjs9pnKnq9qX68ImXvOGz2JuFoEiCjT8jyZQX2z" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/fontawesome.css" integrity="sha384-HbmWTHay9psM8qyzEKPc8odH4DsOuzdejtnr+OFtDmOcIVnhgReQ4GZBH7uwcjf6" crossorigin="anonymous">

        <title>{{ env('APP_NAME') }}</title>
    </head>
    <body>
        <div class="content">
            <header>
                <nav>
                    <h1 class="logo">
                        <a href="/">{{ env('APP_NAME') }}</a>
                    </h1>
                </nav>
            </header>
            <section class="search-item">
                <form>
                    <input type="text" name="term" placeholder="Type to search...">
                </form>
            </section>
            <section class="items-wrapper">
                <h3 class="no-items">There are no items</h3>
                <div class="add-new-item">
                    <a href="javascript:;" title="Add Item"><i class="fas fa-plus"></i></a>
                </div>
                <div class="total-items"></div>
            </section>
        </div>

        <footer>
            <p>My Store &copy; 2018. All rights reserved.</p>
        </footer>

        @include('partials._loader')
        @include('partials._show-item-modal')
        @include('partials._edit-item-modal')
        @include('partials._add-new-item-modal')
        @include('partials._item-template')

        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
