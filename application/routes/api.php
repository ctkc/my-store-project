<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'items'], function () {
    Route::get('/search', 'ItemsController@search');
    Route::get('/', 'ItemsController@index');
    Route::post('/save-order', 'ItemsController@saveOrder');
    // The update route is POST instead of PUT due to Laravel issue with 'multipart/form-data'
    // https://github.com/laravel/framework/issues/13457
    // Another solution could be send file in base64
    Route::post('/{id}', 'ItemsController@update');
    Route::post('/', 'ItemsController@store');
    Route::get('/{id}', 'ItemsController@show');
    Route::delete('/{id}', 'ItemsController@destroy');
});
