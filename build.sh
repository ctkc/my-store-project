#!/bin/sh

if [ $# -gt 0 ]; then
    if [ $1 == "env" ]; then
        if [ ! -e .env ]; then
             cp .env.example .env \
             && php artisan key:generate \
        else
             echo "The .env file already exists. Not regenerated."
        fi
    fi
fi

# Always execute php-fpm command.
php-fpm



