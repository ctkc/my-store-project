#!/bin/sh

COMPOSE="docker-compose"

if [ "$1" == "stop" ]; then
    $COMPOSE down
else
    $COMPOSE up -d \
    && $COMPOSE run --rm app composer install \
    && $COMPOSE run --rm app php artisan config:cache \
    && $COMPOSE run --rm app php artisan migrate \
    && $COMPOSE run --rm assets npm install \
    && $COMPOSE run --rm assets npm run watch
fi