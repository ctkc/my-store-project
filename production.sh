#!/bin/sh

# APP Config
export LARAVEL_ENV=production
export LARAVEL_DEBUG=false
export NODE_ENV=production
export APP_PORT=80

COMPOSE="docker-compose"

if [ "$1" == "stop" ]; then
    $COMPOSE down
else
    $COMPOSE up -d \
    && $COMPOSE run --rm app composer install --optimize-autoloader --no-dev \
    && $COMPOSE run --rm app php artisan config:cache \
    && $COMPOSE run --rm app php artisan migrate --force \
    && $COMPOSE run --rm assets npm install \
    && $COMPOSE run --rm assets npm run production
fi